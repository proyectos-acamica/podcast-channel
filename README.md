# Podcast Channel

Este es el proyecto del primer bloque de la carrera: Desarrollo Web FullStack.

### Pre-requisitos 📋

Unicamente es necesario bases fundamentales en hojas de estilos css con Sass, tambien el uso de la metodología BEM y manejo de estructuras HTML

_Ejemplo:_
```
<p class="home__description--mobile">Un podcast que explora el mundo de la programación. Nuevos episodios, todos los jueves cada 15 días.</p>
```

## Construido con 🛠️

Estas fueron las herramientas usadas para el desarrollo de este proyecto:

* [Sass](https://sass-lang.com/) - El framework css usado
* [BEM](http://getbem.com/) - Metodología Css
* [Fontawesome](https://fontawesome.com/) - Fuente para icons
* [Google Fonts](https://fonts.google.com/) - Fuentes
* [Vercel](https://vercel.com/) - Plataforma en la nube

## Versionado 📌

La indea es implementar [SemVer](http://semver.org/) para el versionado.

## Licencia 📄

Este proyecto está bajo la Licencia  MIT .


---
⌨️ por [David Moreno](https://www.linkedin.com/in/davidmorenocode/) 🤓☕
